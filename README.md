/**********/
/***v1.2***/
/**********/
PAQUETES NPM
Lo mismo que la versión 1.1.

COMANDO EMPLEADO
pug hojaFinal.pug -- --pretty --watch --out tmp
added-> npm run gulp //tarea por defecto
added-> npm run gulp -- watch //control constante de cambios

RESULTADO
Lo mismo que la versión 1.1.
added-> crea un html a través de una plantilla pug empleando gulp.

OTROS
NOTAS
Lo mismo que la versión 1.0.

/**********/
/***v1.1***/
/**********/
PAQUETES NPM
Los locales, ejecutar el siguiente comando para instalarlos:
npm install --only=production

COMANDO EMPLEADO
pug hojaFinal.pug -- --pretty --watch --out tmp

RESULTADO
OTROS
NOTAS
Lo mismo que la versión 1.0.

/**********/
/***v1.0***/
/**********/
PAQUETES NPM
Emplea únicamente el paquete de npm "pug-cli" en global para funcionar
npm install -g pug-cli
		
COMANDO EMPLEADO
pug hojaFinal.pug --pretty --watch --out tmp

RESULTADO	
Con el comando anterior consigues los siguiente:
pug: un perro.
hojaFinal.pug: fichero objetivo para ser compilado. Se podría incluir más.
--pretty: que el fichero html resultante quede formateado.
--watch: que los ficheros de origen sean vigilados y ante cualquier cambio se compilen.
--out tmp: se establece un destino, el producto resultante se almacena aquí.
		
Más funcionalidades aquí:
https://www.npmjs.com/package/pug-cli

OTROS
	Al seleccionar un archivo con dependencias el propio pug-cli las incluye como archivos a seguir para el compilado final, así que también los tiene en cuenta.
	Una vez introducido el watch no se podrá ejecutar más comandos. Es necesario cancelarlo con "ctrl + c", o abrir otra consola.

NOTAS
	Cuando me he querido dar cuenta tenía dos ramas master, una creada en bitbucket con un readme y otra en local con el proyecto, así que he decidido fusionarlas con los siguientes comandos.
	cd herencia_pug_repo/*.pug (donde estan los pugs)
	git remote add principal https://AaronPozo@bitbucket.org/AaronPozo/autopug.git (añadimos el repositorio remoto y lo llamamos principal)
	git pull principal master (bajamos la información al local)
	git merge --allow-unrelated-histories project-a/master # or whichever branch you want to merge (me ha solucionado un error que me saltaba al fusionar)