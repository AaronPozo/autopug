var gulp = require('gulp');
var pug2html = require('gulp-pug');

gulp.task('default', function buildHTML() {
    return gulp.src('hojaFinal.pug')
        .pipe(pug2html())
        .pipe(gulp.dest('tmp/'))
});

gulp.task('watch', function() {
    gulp.watch('*.pug', ['default']);
});